import React from 'react';
import socketIOClient from "socket.io-client";
import { WithStyles, Theme, withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import Avatar from '@material-ui/core/Avatar';
import CircularProgress from '@material-ui/core/CircularProgress';
import { blue } from '@material-ui/core/colors';
import IWeatherData from '../../interfaces/weatherData.interfaces';

const mock = [
    {
        response: {
            "latitude": -33.4163937,
            "longitude": -70.9569362,
            "timezone": "America/Santiago",
            "currently": {
                "time": 1563479187,
                "summary": "Partly Cloudy",
                "icon": "partly-cloudy-day",
                "nearestStormDistance": 0,
                "precipIntensity": 0,
                "precipProbability": 0,
                "temperature": 54.97,
                "apparentTemperature": 54.97,
                "dewPoint": 39.81,
                "humidity": 0.57,
                "pressure": 1020.48,
                "windSpeed": 3.62,
                "windGust": 3.62,
                "windBearing": 159,
                "cloudCover": 0.21,
                "uvIndex": 1,
                "visibility": 4.346,
                "ozone": 297.7
            }
        }
    }
];

type Props = WithStyles<"text" | "paper" | "list" | "subheader" | "grow" | "fabButton" | "progress">;

const styles: any = (theme: Theme) => ({
    text: {
        padding: theme.spacing(2, 2, 0),
    },
    paper: {
        paddingBottom: 50,
    },
    list: {
        marginBottom: theme.spacing(2),
    },
    subheader: {
        backgroundColor: theme.palette.background.paper,
    },
    grow: {
        flexGrow: 1,
    },
    progress: {
        margin: theme.spacing(2),
        color: blue[500]
    },
    fabButton: {
        position: 'absolute',
        zIndex: 1,
        top: -30,
        left: 0,
        right: 0,
        margin: '0 auto',
    },
});

const initialState = {
    loading: true,
    data: mock
};

type State = Readonly<typeof initialState>;

const endPoint: string = 'http://127.0.0.1:8000';

class WeatherComponent extends React.Component<Props, State>
{
    readonly state: State = initialState;

    constructor(props: Props) {
        super(props);
    }

    componentDidMount() {
        const socket = socketIOClient(endPoint);
        socket.on("socketResponse", (data: Array<IWeatherData>) => this.setState({ loading: false, data }));
    }

    render(): JSX.Element {
        console.log(this.state)
        return (
            <React.Fragment>
                <CssBaseline />
                <Paper square className={this.props.classes.paper}>
                    <Typography className={this.props.classes.text} variant="h5" gutterBottom>
                        Ciudades
                        </Typography>
                    {this.state.loading ? <CircularProgress className={this.props.classes.progress} thickness={7} /> :
                        <List className={this.props.classes.list}>
                            {this.state.data.map(({ response }) => (
                                <React.Fragment key={response.latitude + response.longitude}>
                                    <ListSubheader className={this.props.classes.subheader}>{response.timezone}</ListSubheader>
                                    <ListItem button>
                                        <ListItemAvatar>
                                            <Avatar alt="Profile Picture" src={`./images/${response.currently.icon}.png`} />
                                        </ListItemAvatar>
                                        <ListItemText primary={response.currently.summary} secondary={`Temperatura: ${response.currently.temperature} ℃`} />
                                    </ListItem>
                                </React.Fragment>
                            ))}
                        </List>
                    }
                </Paper>
            </React.Fragment >
        );
    }
}

export default withStyles(styles)(WeatherComponent);