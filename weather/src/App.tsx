import React from 'react';
import HeaderComponent from './components/layaout/header.component';
import WeatherComponent from './components/weather/weather.component';
import { Grid, CssBaseline, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  appFrame: {
    zIndex: 1,
    overflow: "hidden",
    width: "100%",
    height: "100%",
    minHeight: 270
  }
}));

const App: React.FC = () => {
  const classes = useStyles();
  return (
    <Grid container className={classes.root}>
      <CssBaseline />
      <HeaderComponent />
      <Grid className={classes.appFrame}>
        <WeatherComponent />
      </Grid>
    </Grid>
  );
}

export default App;
