interface IWeatherData {
   response: {
      latitude: number;
      longitude: number;
      timezone: string;
      currently: {
         apparentTemperature: number;
         cloudCover: number;
         dewPoint: number;
         humidity: number;
         icon: string;
         ozone: number;
         precipIntensity: number;
         precipProbability: number;
         pressure: number;
         summary: string;
         temperature: number;
         time: number;
         uvIndex: number;
         visibility: number;
         windBearing: number;
         windGust: number;
         windSpeed: number;
         precipType: string;
         nearestStormDistance: number | 0;
      }
   }
}


export default IWeatherData;