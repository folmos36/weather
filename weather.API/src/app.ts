import * as express from 'express';
import * as bodyParser from 'body-parser';
import socketIo from 'socket.io';
import dotenv from 'dotenv';
import { RedisClient } from './redis.client';
import HttpException from './exceptions/httpException';
import http, { Server } from 'http';
import { WeatherController } from './controllers/weather.controller';
/**
 * Config
 */
dotenv.config();
const appPort: number = parseInt(<string>process.env.PORT, 10) || 8000;

export class App {
  /**
   * private fields
   */
  private router: express.Router;
  private _redisClient: RedisClient;
  private _weatherController: WeatherController;
  /**
   * @param app - express application
   */
  constructor(private app: express.Express) {
    this.app.use(bodyParser.urlencoded({ limit: "10mb", extended: true }));
    this.app.use(bodyParser.json({ limit: "10mb" }));
    this.router = express.Router();

    this._redisClient = RedisClient.getInstance();
    this.configureRoutes();
    this._weatherController = new WeatherController();
  }

  private errorHandling(error: HttpException, req: express.Request,
    res: express.Response, next: express.NextFunction): void {
    const status = error.status || 500;
    const message = error.message || 'Something went wrong';
    res.status(status);
    res.json({
      error: {
        message
      }
    });
  }
  /**
   * routes configuration
   */
  private configureRoutes(): void {
    this.app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers",
        "Access-Control-Allow-Headers,Origin," +
        "Accept,X-Requested-With, Content-Type,Access-Control-Request-Method," +
        "Access-Control-Request-Headers,Authorization,code,currency,name,id");
      res.header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
      next();
    });
    this.router.get("/", (req: express.Request,
      response: express.Response) => {
      response.send("welcome to weather api!");
    }
    );
    //this.router.use("/weather", weatherRoute);
    this.router.use((error: HttpException,
      req: express.Request,
      res: express.Response,
      next: express.NextFunction) => this.errorHandling(error, req, res, next));
    this.app.use("/api", this.router);
  }

  /**
   * start point app
   */
  public run(): void {
    let interval: NodeJS.Timeout;
    const httpServer: Server = new http.Server(this.app);
    const io = socketIo(httpServer);
    io.on("connection", socket => {
      console.log("New client connected");
      if (interval) {
        clearInterval(interval);
      }
      interval = setInterval(() => {
        this._weatherController.getAll(socket);
      }, 10000);
      socket.on("disconnect", () => {
        console.log("Client disconnected");
      });
    });
    httpServer.listen(appPort, () => {
      this._redisClient.run();
      const address: any = httpServer.address();
      console.log(`==> 🌎 Listening on ${address.port}. Open up http://localhost:${address.port}/ in your browser.`);
    });
  }
}
