import { RedisClient } from "../redis.client";
import { Observable, of, forkJoin, throwError, timer } from "rxjs";
import { switchMap, map, mergeMap, flatMap, retryWhen, tap, delayWhen } from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax'
import ICiudad from "../interfaces/ciudad.interface";
import APIException from "../exceptions/apiException";

const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

function createXHR() {
    return new XMLHttpRequest();
}

export class WeatherController {

    getAll(socket: SocketIO.Socket): void {
        RedisClient.getInstance()
            .search(`${process.env.LOCATIONS_KEY}`)
            .pipe(
                switchMap((redisResult: string) => {
                    if (Math.random() < 0.1) {
                        throw new APIException(500, 'How unfortunate! The API Request Failed');
                    } else {
                        let ciudades: ICiudad[] = JSON.parse(redisResult);
                        let uris: Observable<AjaxResponse>[] = [];
                        ciudades.map(el => {
                            uris.push(ajax({
                                createXHR,
                                url: `${process.env.API_URL}/${process.env.API_KEY}/${el.Lat},${el.Long}`,
                                crossDomain: true,
                                withCredentials: false,
                                method: 'GET'
                            }))
                        });
                        return forkJoin(...uris)
                    }
                }),
                retryWhen(errors =>
                    errors.pipe(
                        tap(errors => {
                            if (errors instanceof APIException) {
                                RedisClient.getInstance().insert(`${process.env.ERROR_KEY}`, errors);
                            }
                        }),
                        delayWhen(() => timer(1000))
                    )
                )).subscribe(
                    (resp: any) => {
                        socket.emit('socketResponse', resp)
                    }
                );
    }
} 