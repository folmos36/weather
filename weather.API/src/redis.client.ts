import * as redis from 'redis';
import localidades from './localidades';
import * as Rx from "rxjs";
import APIException from './exceptions/apiException';

export class RedisClient {

    private static instance: RedisClient;
    private _client: redis.RedisClient;

    private constructor() {
        let redisPort: number = parseInt(<string>process.env.REDIS_PORT, 10) || 3000;
        this._client = redis.createClient(redisPort, process.env.REDIS_HOST);
    }

    static getInstance(): RedisClient {
        if (!RedisClient.instance) {
            RedisClient.instance = new RedisClient();
        }
        return RedisClient.instance;
    }

    run(): void {

        this._client.on('connect', () => {
            console.log(`===> Redis listening on ${process.env.REDIS_HOST}:${process.env.REDIS_PORT}`);
        });

        this._client.on('error', (error) => {
            if (error) {
                console.log("===> Error: " + error);
            }
        })
        this.insert(`${process.env.LOCATIONS_KEY}`, JSON.stringify(localidades.Ciudades));
    }

    insert(key: string, value: any): void {
        if (value instanceof APIException) {
            this._client.hmset(key, { 'datetime': value.datetime.toString(), 'message': value.message, 'code': value.status.toString() });
        } else {
            this._client.set(key, value);
        }
    }

    search(key: string): Rx.Observable<string> {

        return Rx.Observable.create((observer: Rx.Observer<string>) => {
            this._client.get(`${key}`, (error, result) => {
                if (error) {
                    observer.error(`===> Error: searching in redis (key:${key}): ${error}`);
                }
                observer.next(result);
            })
        });
    }
}