interface ICiudad {
    Ciudad: string;
    Pais: string;
    Lat: string;
    Long: string;
};

export default ICiudad;