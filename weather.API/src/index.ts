    
import express from "express";
import { App } from "./app";

let api: App = new App(express());
api.run()