class APIException extends Error {
    status: number;
    message: string;
    datetime: Date = new Date();
    constructor(status: number, message: string) {
      super(message);
      this.status = status;
      this.message = message;
    }
  }
   
  export default APIException;