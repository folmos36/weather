export default {
    Ciudades: [
        {
            Ciudad: "Santiago",
            Pais: "CL",
            Lat: "-33.4163937",
            Long: "-70.9569362"
        },
        {
            Ciudad: "Zúrich",
            Pais: "CH",
            Lat: "47.3775499",
            Long: "8.4666748"
        },
        {
            Ciudad: "Auckland",
            Pais: "NZ",
            Lat: "-36.8621448",
            Long: "174.5852775"
        },
        {
            Ciudad: "Sydney",
            Pais: "AU",
            Lat: "-33.8473567",
            Long: "150.651781"
        },
        {
            Ciudad: "Londres",
            Pais: "UK",
            Lat: "51.5287352",
            Long: "-0.3817848"
        },
        {
            Ciudad: "Georgia",
            Pais: "US",
            Lat: "32.6627783",
            Long: "-85.4225709"
        },
    ]
};